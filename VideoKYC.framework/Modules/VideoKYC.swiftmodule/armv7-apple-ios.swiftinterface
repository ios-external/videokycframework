// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name VideoKYC
import AWSCognitoIdentityProvider
import AWSCore
import AWSKinesisVideo
import AWSKinesisVideoSignaling
import AWSMobileClient
import CommonCrypto
import CoreLocation
import Foundation
import PureLayout
import Starscream
import Swift
import UIKit
@_exported import VideoKYC
import WebRTC
@objc public protocol TimerViewDelegate : AnyObject {
  @objc optional func timerDidUpdateCounterValue(sender: VideoKYC.TimerView, newValue: Swift.Int)
  @objc optional func timerDidStart(sender: VideoKYC.TimerView)
  @objc optional func timerDidPause(sender: VideoKYC.TimerView)
  @objc optional func timerDidResume(sender: VideoKYC.TimerView)
  @objc optional func timerDidEnd(sender: VideoKYC.TimerView, elapsedTime: Foundation.TimeInterval)
}
@objc @_inheritsConvenienceInitializers public class TimerView : UIKit.UIView {
  @objc @IBInspectable public var lineWidth: CoreGraphics.CGFloat
  @objc @IBInspectable public var lineColor: UIKit.UIColor
  @objc @IBInspectable public var trailLineColor: UIKit.UIColor
  @objc @IBInspectable public var isLabelHidden: Swift.Bool
  @objc @IBInspectable public var labelFont: UIKit.UIFont?
  @objc @IBInspectable public var labelTextColor: UIKit.UIColor
  @objc @IBInspectable public var timerFinishingText: Swift.String?
  weak public var delegate: VideoKYC.TimerViewDelegate?
  public var useMinutesAndSecondsRepresentation: Swift.Bool
  public var moveClockWise: Swift.Bool
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public func draw(_ rect: CoreGraphics.CGRect)
  public func start(beginingValue: Swift.Int, interval: Foundation.TimeInterval = 1)
  public func pause()
  public func resume()
  public func reset()
  public func end()
  @objc deinit
}
public protocol KVideoKYCSchedulingDelegate {
  func isVideoKYCCallScheduled(completionState: Swift.Bool, result: [Swift.String : Any]?)
}
@_hasMissingDesignatedInitializers public class Event {
  public class func parseEvent(event: Swift.String) -> VideoKYC.Message?
  @objc deinit
}
extension String {
  public func base64Encoded() -> Swift.String?
  public func base64Decoded() -> Swift.String?
  public func convertToDictionaryValueAsString() throws -> [Swift.String : Swift.String]
}
public protocol KVideoKYCCompletionDelegate {
  func getVideoKYCState(completionState: Swift.String, result: [Swift.String : Any]?)
}
@objc @_hasMissingDesignatedInitializers public class VKYCViewController : UIKit.UIViewController, CoreLocation.CLLocationManagerDelegate, VideoKYC.TimerViewDelegate, VideoKYC.KVideoKYCSchedulingDelegate {
  public func isVideoKYCCallScheduled(completionState: Swift.Bool, result: [Swift.String : Any]?)
  public var delegate: VideoKYC.KVideoKYCCompletionDelegate?
  public init(userToken: Swift.String, environment: Swift.String, transactionId: Swift.String)
  @objc override dynamic public func viewDidLoad()
  @objc public func locationManager(_ manager: CoreLocation.CLLocationManager, didUpdateLocations locations: [CoreLocation.CLLocation])
  @objc public func timerDidUpdateCounterValue(sender: VideoKYC.TimerView, newValue: Swift.Int)
  @objc deinit
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
}
extension URLRequest {
  public func cURL(pretty: Swift.Bool = false) -> Swift.String
}
public class Message : Swift.Codable {
  public init(_ action: Swift.String, _ senderClientId: Swift.String, _ messagePayload: Swift.String)
  public init(_ action: Swift.String, _ recipientClientId: Swift.String, _ senderClientId: Swift.String, _ messagePayload: Swift.String)
  public func getAction() -> Swift.String
  public func setAction(action: Swift.String)
  public func getRecipientClientId() -> Swift.String
  public func setRecipientClientId(recipientClientId: Swift.String)
  public func getSenderClientId() -> Swift.String
  public func setSenderClientId(senderClientId: Swift.String)
  public func getMessagePayload() -> Swift.String
  public func setMessagePayload(messagePayload: Swift.String)
  public class func createAnswerMessage(sdp: Swift.String, _ recipientClientId: Swift.String) -> VideoKYC.Message
  public class func createOfferMessage(sdp: Swift.String, senderClientId: Swift.String) -> VideoKYC.Message
  public class func createIceCandidateMessage(candidate: WebRTC.RTCIceCandidate, _ master: Swift.Bool, recipientClientId: Swift.String, senderClientId: Swift.String) -> VideoKYC.Message
  required public init(from decoder: Swift.Decoder) throws
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
}
